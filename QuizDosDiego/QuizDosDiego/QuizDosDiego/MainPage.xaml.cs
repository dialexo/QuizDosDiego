﻿using QuizDosDiego.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuizDosDiego
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void Guardarcomida(object sender, EventArgs e)
        {



            if (string.IsNullOrEmpty(entrycomida.Text))
            {


                DisplayAlert("Error", "Debe Escribir Tipo de comida", "Ok");

            }
            else
            {

                 Comidas comida = new Comidas()
                {
                    nombre_comida = entrycomida .Text,

                };

                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
                {
                    connection.CreateTable<Comidas>();

                    // crear registro en la tabla
                    var result = connection.Insert(comida);

                    if (result > 0)
                    {
                        DisplayAlert("Correcto", "El producto fue registrado", "OK");
                    }
                    else
                    {
                        DisplayAlert("InCorrecto", "El producto No fue registrado", "OK");
                    }

                    List<Comidas> listaComidas;
                    listaComidas = connection.Table<Comidas>().ToList();

                    listaComida.ItemsSource = listaComidas;

                    entrycomida.Text = "";

                }

            }

        }
    }
}
