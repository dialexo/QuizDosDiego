﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace QuizDosDiego
{
	public partial class App : Application
	{
        public static String urlBd;
        public App ()
		{
			InitializeComponent();

			MainPage = new QuizDosDiego.MainPage();
		}

        // creamos la nueva

        public App(string url)
        {
            InitializeComponent();

            urlBd = url;

            MainPage = new QuizDosDiego.MainPage();
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
